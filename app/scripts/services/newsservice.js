'use strict';

/**
 * @ngdoc service
 * @name hackerNewsAppApp.newsService
 * @description
 * # newsService
 * Provider in the hackerNewsAppApp.
 */
angular.module('hackerNewsAppApp')
  .provider('newsService', function () {
    //
    // // Private variables
    var latestTopStories = [];


      this.$get = ['$http', function ($http) {
          return {
              getTopStories: function (props, success, error) {

                  $http.get('https://hacker-news.firebaseio.com/v0/newstories.json?print=pretty')
                      .then(
                          function (response) {
                              latestTopStories = response.data;
                              success(response.data);
                          },
                          function (response) {
                              error(response.data);
                          });
              },
              getTopStorieDetails: function (props, success, error) {
                  // debugger;
                  if(!props.limit){props.limit = 30;}
                  if(!props.pg){props.pg = 0;}
                  if(!latestTopStories)return;
                  var start = props.pg * props.limit;
                  var end = (props.pg+1) * props.limit;

                  var ids =  latestTopStories.slice(start, end );
                  // console.log(ids);

                  var context = 0;
                  var newData = [];
                  for(var index=0;index<ids.length;index++){
                      var element = ids[index]

                      $http.get('https://hacker-news.firebaseio.com/v0/item/'+element+'.json?print=pretty')
                          .then(
                              function (response) {
                                  newData.push(response.data); //{ response.data.id : response.data}
                                  if(++context == ids.length){
                                      success(JSON.parse(JSON.stringify(newData)));
                                  }
                              },
                              function (response) {
                                  error(response.data);
                              });

                  }



              }
          }
      }]
  });
