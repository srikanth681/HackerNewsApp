'use strict';

/**
 * @ngdoc overview
 * @name hackerNewsAppApp
 * @description
 * # hackerNewsAppApp
 *
 * Main module of the application.
 */
angular
  .module('hackerNewsAppApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'infinite-scroll',
    'yaru22.angular-timeago',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/news', {
        templateUrl: 'views/news.html',
        controller: 'NewsCtrl',
        controllerAs: 'news'
      })
      .when('/topStories', {
        templateUrl: 'views/topstories.html',
        controller: 'TopstoriesCtrl',
        controllerAs: 'topStories'
      })
      .when('/bestStories', {
        templateUrl: 'views/beststories.html',
        controller: 'BeststoriesCtrl',
        controllerAs: 'bestStories'
      })
      .when('/askStories', {
        templateUrl: 'views/askstories.html',
        controller: 'AskstoriesCtrl',
        controllerAs: 'askStories'
      })
      .when('/showStories', {
        templateUrl: 'views/showstories.html',
        controller: 'ShowstoriesCtrl',
        controllerAs: 'showStories'
      })
      .when('/jobStories', {
        templateUrl: 'views/jobstories.html',
        controller: 'JobstoriesCtrl',
        controllerAs: 'jobStories'
      })
      .otherwise({
        redirectTo: '/news'
      });
  });
