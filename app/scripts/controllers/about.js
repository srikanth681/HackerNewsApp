'use strict';

/**
 * @ngdoc function
 * @name hackerNewsAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the hackerNewsAppApp
 */
angular.module('hackerNewsAppApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
