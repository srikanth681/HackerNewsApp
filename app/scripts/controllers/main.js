'use strict';

/**
 * @ngdoc function
 * @name hackerNewsAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the hackerNewsAppApp
 */
angular.module('hackerNewsAppApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
