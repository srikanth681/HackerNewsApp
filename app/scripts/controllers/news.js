'use strict';

/**
 * @ngdoc function
 * @name hackerNewsAppApp.controller:NewsCtrl
 * @description
 * # NewsCtrl
 * Controller of the hackerNewsAppApp
 */
angular.module('hackerNewsAppApp')
  .controller('NewsCtrl', function ($scope, newsService) {

    $scope.newsList = [];
    $scope.showWebsite = function(url){
        return extractDomain(url);
    }
    var pg = 1, limit = 10;

    $scope.loadMore = function() {
        // return;
        pg++;
        newsService.getTopStorieDetails({"pg":pg,'limit':limit}, function(results){
                // console.log(results);
                // $scope.newsList = results;
                if(results)
                if(results.length)
                    Array.prototype.push.apply( $scope.newsList,results)
            }, function(error){
                console.log(error);
            }
        )
    };

    newsService.getTopStories({}, function(results){
        // console.log(results);
        newsService.getTopStorieDetails({"pg":1,'limit':10}, function(results){
                // console.log(results);
                $scope.newsList = results;
            }, function(error){
                console.log(error);
            }
        )
        
    }, function(error){
        console.log(error);
        }

    )
      
      
  });
function extractDomain(url) {
    if(!url)return;
    var domain;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
    }
    else {
        domain = url.split('/')[0];
    }

    //find & remove port number
    domain = domain.split(':')[0];
    domain = domain.replace('www.','');

    return domain;
}
