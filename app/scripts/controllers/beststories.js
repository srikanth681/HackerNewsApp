'use strict';

/**
 * @ngdoc function
 * @name hackerNewsAppApp.controller:BeststoriesCtrl
 * @description
 * # BeststoriesCtrl
 * Controller of the hackerNewsAppApp
 */
angular.module('hackerNewsAppApp')
  .controller('BeststoriesCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
