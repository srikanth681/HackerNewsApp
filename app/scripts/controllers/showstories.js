'use strict';

/**
 * @ngdoc function
 * @name hackerNewsAppApp.controller:ShowstoriesCtrl
 * @description
 * # ShowstoriesCtrl
 * Controller of the hackerNewsAppApp
 */
angular.module('hackerNewsAppApp')
  .controller('ShowstoriesCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
