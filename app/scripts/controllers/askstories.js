'use strict';

/**
 * @ngdoc function
 * @name hackerNewsAppApp.controller:AskstoriesCtrl
 * @description
 * # AskstoriesCtrl
 * Controller of the hackerNewsAppApp
 */
angular.module('hackerNewsAppApp')
  .controller('AskstoriesCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
