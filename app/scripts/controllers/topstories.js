'use strict';

/**
 * @ngdoc function
 * @name hackerNewsAppApp.controller:TopstoriesCtrl
 * @description
 * # TopstoriesCtrl
 * Controller of the hackerNewsAppApp
 */
angular.module('hackerNewsAppApp')
  .controller('TopstoriesCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
