'use strict';

/**
 * @ngdoc function
 * @name hackerNewsAppApp.controller:JobstoriesCtrl
 * @description
 * # JobstoriesCtrl
 * Controller of the hackerNewsAppApp
 */
angular.module('hackerNewsAppApp')
  .controller('JobstoriesCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
