'use strict';

describe('Controller: BeststoriesCtrl', function () {

  // load the controller's module
  beforeEach(module('hackerNewsAppApp'));

  var BeststoriesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BeststoriesCtrl = $controller('BeststoriesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(BeststoriesCtrl.awesomeThings.length).toBe(3);
  });
});
