'use strict';

describe('Controller: ShowstoriesCtrl', function () {

  // load the controller's module
  beforeEach(module('hackerNewsAppApp'));

  var ShowstoriesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShowstoriesCtrl = $controller('ShowstoriesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ShowstoriesCtrl.awesomeThings.length).toBe(3);
  });
});
