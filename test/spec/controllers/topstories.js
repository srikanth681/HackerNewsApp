'use strict';

describe('Controller: TopstoriesCtrl', function () {

  // load the controller's module
  beforeEach(module('hackerNewsAppApp'));

  var TopstoriesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TopstoriesCtrl = $controller('TopstoriesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TopstoriesCtrl.awesomeThings.length).toBe(3);
  });
});
