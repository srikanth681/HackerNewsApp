'use strict';

describe('Controller: JobstoriesCtrl', function () {

  // load the controller's module
  beforeEach(module('hackerNewsAppApp'));

  var JobstoriesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    JobstoriesCtrl = $controller('JobstoriesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(JobstoriesCtrl.awesomeThings.length).toBe(3);
  });
});
