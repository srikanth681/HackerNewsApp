'use strict';

describe('Controller: AskstoriesCtrl', function () {

  // load the controller's module
  beforeEach(module('hackerNewsAppApp'));

  var AskstoriesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AskstoriesCtrl = $controller('AskstoriesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AskstoriesCtrl.awesomeThings.length).toBe(3);
  });
});
