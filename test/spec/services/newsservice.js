'use strict';

describe('Service: newsService', function () {

  // instantiate service
  var newsService,
    init = function () {
      inject(function (_newsService_) {
        newsService = _newsService_;
      });
    };

  // load the service's module
  beforeEach(module('hackerNewsAppApp'));

  it('should do something', function () {
    init();

    expect(!!newsService).toBe(true);
  });

  it('should be configurable', function () {
    module(function (newsServiceProvider) {
      newsServiceProvider.setSalutation('Lorem ipsum');
    });

    init();

    expect(newsService.greet()).toEqual('Lorem ipsum');
  });

});
